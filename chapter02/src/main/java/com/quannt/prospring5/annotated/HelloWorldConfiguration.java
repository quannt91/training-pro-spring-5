package com.quannt.prospring5.annotated;

import com.quannt.prospring5.decoupled.HelloWorldMessageProvider;
import com.quannt.prospring5.decoupled.MessageProvider;
import com.quannt.prospring5.decoupled.MessageRenderer;
import com.quannt.prospring5.decoupled.StandardOutMessageRenderer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class HelloWorldConfiguration {

    @Bean
    public MessageProvider provider() {
        return new HelloWorldMessageProvider();
    }

    @Bean
    public MessageRenderer renderer(){
        MessageRenderer renderer = new StandardOutMessageRenderer();
        renderer.setMessageProvider(provider());
        return renderer;
    }
}

