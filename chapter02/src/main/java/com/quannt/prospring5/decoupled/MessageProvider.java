package com.quannt.prospring5.decoupled;

public interface MessageProvider {
    String getMessage();
}
